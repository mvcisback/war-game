from player import Player

class AI(Player):
    def __init__(self, color, search):
        """
        """
        Player.__init__(self, color)
        self._algorithm = search


    def move(self, board):
        return self._algorithm.search(board, self.color)

    @property
    def count(self):
        return self._algorithm.count
