#!/usr/bin/python3

from player import Player
from gameboard import *

MOVES = {'drop', 'blitz'}

class Game(object):
    """
    Representation of game between to players in war.

    Maintains 2 players blue and green (blue goes first).
    """

    def __init__(self, board_name, blue_player, green_player):
        """
        Creates a game object.

        :param board_name: path to the board to play
        :param blue_player: Player for blue (default is User type)
        :param green_player: Player for green (default is User type)
        """
        self._board = GameBoard(board_name)
        self._turns = self._board.width*self._board.height
        self._blue_turn = True
        self._blue, self._green = blue_player, green_player
        self._blue.color, self._green.color = 'BLUE', 'GREEN'


    def turns(self):
        """
        Returns the player who is next.
        - Alternates between blue and green.
        """
        _turns = self._turns
        while _turns != 0:
            yield self._blue if self._blue_turn else self._green
            self._blue_turn = not self._blue_turn
            _turns -= 1


    def play(self):
        """
        Loops through the game until the game is finished.
        Shows board state after each turn.
        """
        for player in self.turns():
            print(chr(27) + "[2J")
            print(self._board)
            coord, move = player.move(self._board)
            # todo check for valid move
            fn = self._board.drop if move == 'drop' else self._board.blitz
            while not fn(coord, player.color):
                print(chr(27) + "[2J")
                print(self._board)
                print('Move failed try again.\n')
                coord, move = player.move(self._board)
                # todo check for valid move
                fn = self._board.drop if move == 'drop' else self._board.blitz

        print(chr(27) + "[2J")
        print("GAME OVER")
        print("The final score is: BLUE: %d, GREEN: %d\n" % self._board.score)
        b_score, g_score = self._board.score
        if b_score == g_score:
            print("Its a tie!")
        else:
            winner = "BLUE" if b_score > g_score else "GREEN"
            print(colorstr("The Winner is %s" % winner, winner) if not
                  self._board.test else "The Winner is %s" % winner)
        print(self._board)
