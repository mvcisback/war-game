#!/usr/bin/python3

from collections import namedtuple
from functools import reduce
from itertools import chain

test = False

#Dictionary and helper functions for printing in color
CCODE={
        'ENDC':0,
        'WHITE':57,
        'BLUE':94,
        'GREEN':92,
        'BG':40,
}

def _termcode(num):
    return '\033[%sm'%num

def colorstr(msg, color):
    return "{color}{bg}{msg}{endc}".format(color=_termcode(CCODE[color]),
                                           bg=_termcode(CCODE['BG']), msg=msg,
                                           endc=_termcode(CCODE['ENDC']))

#set up a Square tuple to hold owner(color) and val(num)
Square = namedtuple("Square", ["owner", "val"])


def _convert_coord(x, y):
    if type(x) == str:
        return ord(x) - ord('A'), y-1
    return x, y


def sanitize_coord(fn):
    def _sanitze_coords(self, coord, *args, **kwds):
        x, y = coord
        x, y = _convert_coord(x, y)
        if x not in range(0, self.width) or y not in range(0, self.height):
            return None
        return fn(self, (x, y), *args, **kwds)
    return _sanitze_coords


def record(fn):
    def _record(self, coord, color, commit=True):
        if commit:
            self._commit.clear()
            ret = fn(self, coord, color)
            self._undo.append(self._commit)
            self._commit = []
        else:
            return fn(self, coord, color)
        return ret
    return _record


def _update_score(score, cell):
    blue, green = score
    if(cell.owner == "BLUE"):
        return blue  + cell.val, green
    elif (cell.owner == "GREEN"):
        return blue, green + cell.val
    return blue, green


class GameBoard:
    """
    Class for a WarGame Board
    initialize with gameboard file
    Set/Get squares like this: INSTANCE_NAME['A', 1]
    Printout class function will print board in color
    """
    def __init__(self, boardfile):
        self.grid = []
        with open(boardfile) as f:
            self.grid = [[Square('WHITE', int(x)) for x in line.split()]
                         for line in f]
        self._h, self._w = len(self.grid), len(self.grid[0])
        self._utility = 0
        self.test = False
        self._filled = 0
        self._commit = []
        self._undo = []


    @sanitize_coord
    def __getitem__(self, coord):
        x, y = coord
        return self.grid[y][x]


    @sanitize_coord
    def __setitem__(self, coord, owner):
        x,y = coord
        old = self.grid[y][x]
        self.grid[y][x] = Square(owner, old.val)
        self._commit.append((coord, old.owner, self._utility))
        return True


    def __str__(self):
        # make top dimensions
        out = reduce(lambda out, dim: out + "  " + chr(ord('A') + dim),
                     range(0, self.width), " ")
        out += "\n"
        for r, row in enumerate(self.grid):
            out += str(r+1) + " "
            for square in row:
                if (self.test):
                    s = square.owner[0] + str(square.val).zfill(2)
                else:
                    s = colorstr(str(square.val).zfill(2),square.owner)
                out += s + " "
            out += "\n"
        return out


    def undo(self):
        """Undo's last move."""
        for coord, owner, utility in self._undo.pop():
            self.__setitem__(coord, owner)
            self._utility = utility
            if owner == "WHITE":
                self._filled -= 1


    @property
    def filled(self):
        """Returns the number of squares that are filled."""
        return self._filled


    @property
    def width(self):
        return self._w


    @property
    def height(self):
        return self._h


    @sanitize_coord
    def neighbors(self, coord):
        """Returns the neighbors of the cell at (x,y).
        The returned List elements are (coord, cell)."""
        x, y = coord
        n = []
        in_bounds = lambda z, bound: z >= 0 and z < bound
        if x-1 in range(0, self.width):
            n.append(((x-1, y), self.grid[y][x-1]))
        if y-1 in range(0, self.height):
            n.append(((x, y-1), self.grid[y-1][x]))
        if x+1 in range(0, self.width):
            n.append(((x+1, y), self.grid[y][x+1]))
        if y+1 in range(0, self.height):
            n.append(((x, y+1), self.grid[y+1][x]))
        return n


    def _occupied(self, cell):
        """Checks if a cell is occupied"""
        return cell.owner != "WHITE"


    @sanitize_coord
    def occupied(self, coord):
        """Returns whether the cell at (x,y) is occupied"""
        x, y = coord
        return self._occupied(self.grid[y][x])


    @property
    def full(self):
        """
        Looks through whole board to see if board is completely occupied
        """
        return self.width * self.height == self.filled


    @property
    def score(self):
        """Sums up the score for each player"""
        cells = chain.from_iterable(self.grid)
        return reduce(_update_score, cells, (0, 0))


    @property
    def utility(self):
        return self._utility


    @sanitize_coord
    @record
    def drop(self, coord, o):
        """
        Para Drop a Commando onto an open space on the board.
        Returns True if successful
        """
        x, y = coord
        if not self.occupied(coord):
            self[x,y] = o
            self._filled += 1
            if o == "BLUE":
                self._utility += self[coord].val
            else:
                self._utility -= self[coord].val
            return True
        return False


    def _occupy_neighbor(self, coord, o):
        for coord, cell in self.neighbors(coord):
            if cell.owner == o:
                return True
        return False


    @sanitize_coord
    @record
    def blitz(self, coord, o):
        """M1 Death Blitz by moving into a """
        if not self._occupy_neighbor(coord, o):
            return False
        if  not self.drop(coord, o, commit=False):
            return False
        for coord, cell in self.neighbors(coord):
            if self.occupied(coord) and self[coord].owner != o:
                self[coord] = o
                if o == "BLUE":
                    self._utility += 2*self[coord].val
                else:
                    self._utility -= 2*self[coord].val
        return True


    def _to_coord(self, x):
        return (chr(x%self.width+ord('A')), x//self.width+1)


    def moves(self, color):
        """
        Return list of possible moves from current game state
        """
        cells = chain.from_iterable(self.grid)
        for x, cell in enumerate(cells):
            if self._occupied(cell):
                continue
            coord = self._to_coord(x)
            if self._occupy_neighbor(coord, color):
                yield (coord, 'blitz')
            yield (coord, 'drop')


class SubBoard(GameBoard):
    @sanitize_coord
    def __init__self(self, coord, orient, board): #'h' for horiz, 'v' for vert
        #self.grid = [][]
        self._w = 3 if (orient == 'h') else 2
        self._h = 2 if (orient == 'h') else 3
        for y in range(0, self._h):
            for x in range(0, self._w):
                self.grid[y][x] = board.grid[coord[1]+y][coord[0]+x]
        self._utility = 0
        self.test = False
        self._filled = 0
        self._commit = []
        self._undo = []
