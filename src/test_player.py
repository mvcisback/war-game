#!/usr/bin/env python3

import unittest
from player import *
from unittest.mock import Mock

class TestPlayerFunctions(unittest.TestCase):

    def setUp(self):
        """
        Setup function
        - create test players
        """
        self.testPlayer = Player('BLUE')


    def test_init(self):
        """
        Test Player.init()
        - Verify inital parameters
        - Verify color setting
        """
        self.assertEqual(self.testPlayer.color, 'BLUE')
        self.testPlayer.color = 'GREEN'
        self.assertEqual(self.testPlayer.color, 'GREEN')


    def test_move(self):
        """
        Test Player.move()
        - check input output processing
        """
        testBoard = Mock()
        testBoard.width = 2
        self.assertEqual(self.testPlayer.move(testBoard),
                          (('A', 2), 'blitz'))


if __name__ == '__main__':
    unittest.main()
