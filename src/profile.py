#!/usr/bin/env python3

import game, ai, cProfile
from search import *

g = None
BOARDS = ["Kalamazoo", "Peoria", "Piqua", "Punxsutawney", "Wallawalla"]


def profile_board(board, search, depth):
    global g
    print("-"*80 + ("\nboard: %s\n" % board) + "-"*80)
    p1 = ai.AI("BLUE", search(depth))
    p2 = ai.AI("GREEN", search(depth))
    g = game.Game(board, p1, p2)
    cProfile.run('g.play()')
    print("P1 Nodes: %s" % p1._algorithm.count)
    print("P2 Nodes: %s" % p2._algorithm.count)


if __name__ == '__main__':
    build_path = lambda board: "../boards/%s.txt" % board
    boards = (build_path(board) for board in BOARDS)

    gen_profiler = (lambda search, depth:
                        lambda board: profile_board(board, search, depth))
    profiles = [
        ("Alpha-Beta", gen_profiler(AlphaBeta, 5)),
        #("Mini-Max", gen_profiler(MiniMax, 3)),
        ("Time-Search", gen_profiler(TimeSearch, 1000)),
        ("Force-Blitz", gen_profiler(ForceBlitz, 5)),
        ("Blitz-Krieg", gen_profiler(BlitzKrieg, 1000))
        ]

    for name, profiler in profiles:
        print("*"*80 + ("\nName: %s" % name) + "-"*80)
        for board in boards:
            print(profiler(board))
