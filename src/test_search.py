#!/usr/bin/env python3

import random
import unittest
from gameboard import GameBoard
from search import *

class TestBoardFunctions(unittest.TestCase):

    def setUp(self):
        self.g = GameBoard("../boards/Peoria.txt")
        self.g.test = True
        self.g2 = GameBoard("../boards/micro.txt")


    def test_utility(self):
        self.assertEqual(utility(self.g), 0)
        self.g.drop(('A', 1), "BLUE")
        self.assertEqual(utility(self.g), 99)
        self.g.drop(('A', 2), "GREEN")
        self.assertEqual(utility(self.g), 98)
        self.g.drop(('A', 4), "BLUE")
        self.assertEqual(utility(self.g), 99)
        self.g.drop(('A', 3), "GREEN")
        self.assertEqual(utility(self.g), 0)
        self.g.drop(('B', 1), "BLUE")
        self.assertEqual(utility(self.g), 1)
        self.g.drop(('A', 5), "GREEN")
        self.assertEqual(utility(self.g), -98)


    def test_minimax(self):
        algo = MiniMax()
        self.micro_search_test(algo)


    def test_alpha_beta(self):
        algo = AlphaBeta()
        self.micro_search_test(algo)


    def micro_search_test(self, algo):
        """
        Check several nodes in search tree for micro.txt
        """
        self.assertTrue( algo.search(self.g2, 'BLUE')
                         in {(('A', 1), "drop"), (('C', 1), "drop")} )

        self.g2.drop(('C', 1), 'BLUE')
        self.assertEqual( algo.search(self.g2, 'GREEN'),
                           (('B', 1), "drop") )

        self.g2.undo()
        self.g2.drop(('B', 1), 'BLUE')
        self.assertEqual( algo.search(self.g2, 'GREEN'),
                           (('C', 1), "drop") )

        self.g2.undo()
        self.g2.drop(('A', 1), 'BLUE')
        self.assertEqual( algo.search(self.g2, 'GREEN'),
                           (('B', 1), "drop") )

        self.g2.drop(('C', 1), 'GREEN')
        self.assertEqual((('B', 1), "blitz"),
                         algo.search(self.g2, 'BLUE'))

        self.g2.undo()
        self.g2.drop(('B', 1), 'GREEN')
        self.assertEqual((('C', 1), "drop"),
                         algo.search(self.g2, 'BLUE'))


if __name__ == '__main__':
    unittest.main()
