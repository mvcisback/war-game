#!/usr/bin/env python3

import glob, readline

from gameboard import *
from player import Player
from game import Game
from ai import AI
from search import *


PTYPE = [("Human", 'Player'),
         ("Computer", 'AI')]

SEARCH = [("MiniMax Search", MiniMax),
          ("AlphaBeta Search", AlphaBeta),
          ("Time Search", TimeSearch),
          ("Force Blitz", ForceBlitz),
          ("Blitz Krieg", BlitzKrieg)]


def menuprompt(prompt, menu):
    print(chr(27) + "[2J")
    sel = None
    while not sel:
        print(prompt)
        print("-"*80)
        for index, item in enumerate(menu):
            print("%d - %s" % (index, item[0]))
        print("-"*80)
        option = input('>> ')
        try:
            sel = menu[int(option)][1]
        except (ValueError, IndexError):
            print(chr(27) + "[2J")
            print("Invalid option.\n")
            sel = None
    return sel


def get_player(prompt, color):
    p = menuprompt(colorstr(prompt, color), PTYPE)
    if p == 'Player':
        return Player(color)
    else:
        print(chr(27) + "[2J")
        while True:
            print(colorstr("Select Difficulty [> 0]", color) + "\n" + "-"*80)
            depth = input('>> ')
            if depth.isdigit() and int(depth) > 0:
                search = menuprompt(colorstr("Select AI Algorithm",
                                             color), SEARCH)(depth)
                return AI(color, search)
            else:
                print(chr(27) + "[2J")
                print("Invalid option.\n")


def complete(text, state):
    return (glob.glob(text+'*')+[None])[state]

def get_board():
    print(chr(27) + "[2J")
    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)
    i = input('Enter Path to Board: ')
    readline.parse_and_bind("tab: ")
    return i


if __name__ == '__main__':
    print("Welcome to the War Game\n")
    p1, p2 = None, None
    while p1 is None:
        p1 = get_player("Select Player 1 (BLUE)", 'BLUE')
    while p2 is None:
        p2 = get_player("Select Player 2 (GREEN)", 'GREEN')
    board = get_board()
    game = Game(board, p1, p2)
    game.play()
