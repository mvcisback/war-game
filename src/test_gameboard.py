#!/usr/bin/env python3

import random
import unittest
from game import Player
from gameboard import GameBoard

class TestBoardFunctions(unittest.TestCase):

    def setUp(self):
        self.g = GameBoard("../boards/Peoria.txt")
        self.g2 = GameBoard("../boards/tiny.txt")

    def test_dim(self):
        self.assertEqual(self.g.width, 5)
        self.assertEqual(self.g.height, 5)


    def test_access(self):
        self.assertEqual(str(self.g['D', 5]), "Square(owner='WHITE', val=1)")
        #out of bounds
        self.assertEqual(self.g['F', 5], None)
        self.assertEqual(self.g['D', 6], None)


    def test_get_set(self):
        self.assertEqual(self.g['A', 1].owner, "WHITE")
        self.assertEqual(self.g['C', 2].owner, "WHITE")
        self.g['A', 1] = "GREEN"
        self.g['C', 2] = "BLUE"
        self.assertEqual(self.g['A', 1].owner, "GREEN")
        self.assertEqual(self.g['C', 2].owner, "BLUE")

        #out of bounds
        self.assertFalse(self.g.__setitem__(('F', 1), "GREEN"))
        self.assertFalse(self.g.__setitem__(('A', -1), "GREEN"))
        self.assertTrue(self.g.__setitem__(('B', 2), "BLUE"))


    def test_neighbors(self):
        expected = {(1, 0), (0, 1)}
        neighbors = [n[0] for n in self.g.neighbors(('A', 1))]
        self.assertEqual(len(neighbors), len(expected))
        self.assertEqual(set(neighbors), expected)
        expected = {(2, 1), (1, 2), (3, 2), (2, 3)}
        neighbors = [n[0] for n in self.g.neighbors(('C', 3))]
        self.assertEqual(len(neighbors), len(expected))
        self.assertEqual(set(neighbors), expected)


    def test_occupied(self):
        self.assertFalse(self.g.occupied(('A', 1)))
        self.g['A', 1] = "GREEN"
        self.assertTrue(self.g.occupied(('A', 1)))
        self.g['A', 1]  = "BLUE"
        self.assertTrue(self.g.occupied(('A', 1)))


    def test_score(self):
        self.g['A', 1] = "GREEN"
        self.g['E', 5] = "GREEN"
        self.g['B', 1] = "BLUE"
        self.g['B', 2] = "BLUE"
        self.assertEqual(self.g.score, (100, 198))


    def test_undo(self):
        self.assertFalse(self.g.occupied(('A', 1)))
        self.g.drop(('A', 1), "GREEN")
        self.assertTrue(self.g.occupied(('A', 1)))
        self.g.undo()
        self.assertFalse(self.g.occupied(('A', 1)))
        self.assertEqual(self.g.filled, 0)
        self.g.drop(('A', 1), "GREEN")
        self.g.drop(('A', 3), "BLUE")
        self.g.blitz(('A', 2), "GREEN")
        self.g.undo()
        self.g.undo()
        self.g.undo()
        self.assertEqual(self.g.filled, 0)


    def test_full(self):
        self.assertFalse(self.g.full)
        self.g.drop(('A', 1), "GREEN")
        self.assertFalse(self.g.full)
        self.g.drop(('A', 1), "GREEN")
        self.assertFalse(self.g.full)
        dims = [(chr(x%5+ord('A')), x//5+1) for x in range(0, self.g.width**2)]
        for dim in dims:
            self.g.drop(dim, "GREEN")
        self.assertTrue(self.g.full)


    def test_drop(self):
        self.assertTrue(self.g.drop(('A', 1), "BLUE"))
        self.assertEqual(self.g['A', 1].owner, "BLUE")
        dims = [(chr(x%5+ord('A')), x//5+1) for x in range(1, self.g.width**2)]
        for dim in dims:
            self.assertEqual(self.g.__getitem__(dim).owner, "WHITE")
        self.assertTrue(self.g.drop(('B', 2), "GREEN"))
        self.assertEqual(self.g['A', 1].owner, "BLUE")
        self.assertEqual(self.g['B', 2].owner, "GREEN")
        self.assertEqual(self.g['B', 1].owner, "WHITE")
        self.assertEqual(self.g['A', 2].owner, "WHITE")
        self.assertTrue(self.g.drop(('A', 2), "BLUE"))
        self.assertEqual(self.g['A', 1].owner, "BLUE")
        self.assertEqual(self.g['B', 2].owner, "GREEN")
        self.assertEqual(self.g['B', 1].owner, "WHITE")
        self.assertEqual(self.g['A', 2].owner, "BLUE")

    def test_blitz(self):
        self.assertFalse(self.g.blitz(('A', 1), "BLUE"))
        self.assertEqual(self.g['A', 1].owner, "WHITE")
        self.assertTrue(self.g.drop(('A', 1), "BLUE"))
        self.assertEqual(self.g['A', 1].owner, "BLUE")
        dims = [(chr(x%5+ord('A')), x//5+1) for x in range(1, self.g.width**2)]
        for dim in dims:
            self.assertEqual(self.g.__getitem__(dim).owner, "WHITE")
        self.assertTrue(self.g.drop(('B', 2), "GREEN"))
        self.assertEqual(self.g['A', 1].owner, "BLUE")
        self.assertEqual(self.g['B', 2].owner, "GREEN")
        self.assertEqual(self.g['B', 1].owner, "WHITE")
        self.assertEqual(self.g['A', 2].owner, "WHITE")
        self.assertTrue(self.g.blitz(('A', 2), "BLUE"))
        self.assertEqual(self.g['A', 1].owner, "BLUE")
        self.assertEqual(self.g['B', 2].owner, "BLUE")
        self.assertEqual(self.g['B', 1].owner, "WHITE")
        self.assertEqual(self.g['A', 2].owner, "BLUE")

    def test_moves(self):
        moves = [(('A', 1), 'drop'),
                 (('B', 1), 'drop'),
                 (('A', 2), 'drop'),
                 (('B', 2), 'drop')]

        p = Player('GREEN')
        for expected, move in zip(moves, self.g2.moves(p.color)):
            self.assertEqual(expected, move)

        self.g2.drop(('B', 2), 'GREEN')
        moves = [(('A', 1), 'drop'),
                 (('B', 1), 'blitz'),
                 (('B', 1), 'drop'),
                 (('A', 2), 'blitz'),
                 (('A', 2), 'drop')]
        for expected, move in zip(moves, self.g2.moves(p.color)):
            self.assertEqual(expected, move)

if __name__ == '__main__':
    unittest.main()
