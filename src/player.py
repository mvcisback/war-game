
from gameboard import *

MOVES = {'drop', 'blitz'}

def _valid(prompt, validator):
    out = input(prompt)
    while not validator(out):
        print("Invalid input. Please re-enter.")
        out = input(prompt)
    return out


class Player(object):
    """
    Base class for players in a game.
    """

    def __init__(self, color):
        self._color = color

    @property
    def color(self): return self._color

    @color.setter
    def color(self, value): self._color = value

    def move(self, board):
        """
        Default movement function.
        Prompts user for letter, number, and move type.

        When overriding ensure that move returns:
        ((letter, num) move_type)
        letter, num is the coordinate specified by the board.
        move_type's are defined in game.MOVES.
        """
        print(colorstr('%s\'S TURN' % self._color, self._color))
        letter = _valid("Pick a letter: ", lambda x: len(x) == 1
            and x.isalpha() and (ord(x.upper())-ord('A')) in range(board.width))
        letter = letter.upper()
        num = _valid("Pick a number: ", lambda x: len(x) == 1 and x.isdigit()
            and int(x) in range(1, board.height+1))
        move_type = _valid("drop or blitz? ",
             lambda x: x in MOVES or x == 'b' or x == 'd')
        if move_type not in MOVES:
            move_type = 'drop' if move_type == 'd' else 'blitz'
        return ((letter, int(num)), move_type)
