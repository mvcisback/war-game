#!/usr/bin/env python3

import unittest
from unittest.mock import Mock
from game import *

class TestGameFunctions(unittest.TestCase):

    def setUp(self):
        """
        Setup function
        - create test players and game (tiny board)
        """
        #self.blue_test = Player('BLUE')
        #self.green_test = Player('GREEN')
        self.blue_test = Mock()
        self.green_test = Mock()
        self.blue_test.color.return_value = 'BLUE'
        self.green_test.color.return_value = 'GREEN'
        self.blue_test.move.side_effect = [(('A', 1), 'drop'), (('B', 2), 'drop')]
        self.green_test.move.side_effect = [(('B', 1), 'drop'), (('A', 2), 'drop')]

        self.testGame = Game("../boards/tiny.txt", self.blue_test, self.green_test)

    def test_init(self):
        """
        Test Game.init()
        - Verify initial parameters
        """
        boardTemp = GameBoard("../boards/tiny.txt")
        self.assertEqual(self.testGame._board.grid, boardTemp.grid)
        self.assertEqual(self.testGame._board._h, boardTemp._h)
        self.assertEqual(self.testGame._board._w, boardTemp._w)
        self.assertEqual(self.testGame._board.test, boardTemp.test)
        self.assertEqual(self.testGame._board._filled, boardTemp._filled)
        self.assertEqual(self.testGame._board._commit, boardTemp._commit)
        self.assertEqual(self.testGame._board._undo, boardTemp._undo)

        self.assertEqual(self.testGame._turns, 4)
        self.assertEqual(self.testGame._blue_turn, True)
        self.assertEqual(self.testGame._blue, self.blue_test)
        self.assertEqual(self.testGame._green, self.green_test)

    def test_turns(self):
        """
        Test Game.turns()
        - call 5 times and assert return values
        """
        players = [self.blue_test, self.green_test]
        self.assertEqual(self.testGame._turns, 4)

        i = 0
        for player in self.testGame.turns():
            self.assertEqual(player.color, players[i%2].color)
            i += 1
        self.assertEqual(i, 4)
        try:
            self.testGame.turns()
        except StopIteration:
            self.assertEqual(1,1)

    def test_play(self):
        """
        Test Game.play()
        - 
        """
        self.testGame._board.test = True
        self.testGame.play()
        print('\nExpected : BLUE: 2, GREEN: 198\n')
        self.assertEqual(self.testGame._board.score, (2, 198))
        self.assertEqual(str(self.testGame._board), '   A  B\n1 B01 G99 \n2 G99 B01 \n')

if __name__ == '__main__':
    unittest.main()
