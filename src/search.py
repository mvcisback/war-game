#!/usr/bin/env python3
"""
File containing Search related functions
"""

import itertools

oo = float('inf')

def utility(board):
    """
    Give a utility
    """
    b, g = board.score
    return b - g


def _toggle_color(color):
    return "GREEN" if color == "BLUE" else "BLUE"


class MiniMax(object):
    """
    """

    def __init__(self, depth=oo):
        self._max_depth = float(depth)
        self.count = 0


    def search(self, board, color):
        return self._search(board, color, self._max_depth)[1]


    def _utility(self, board, color, depth, coord, option):
        getattr(board, option)(coord, color)
        alpha = self._search(board, _toggle_color(color), depth-1)[0]
        board.undo()
        return alpha, (coord, option)


    def _search(self, board, color, depth):
        self.count += 1
        """
        Blue: Maximizes utility
        Green: Minimizes utility
        """
        if depth < 1 or board.full:
            return board.utility, None
        children = (self._utility(board, color, depth, *move) for move in
                    board.moves(color))
        pick = max if color == "BLUE" else min
        return pick(children, key=lambda x: x[0])


class AlphaBeta(MiniMax):
    def __init__(self, depth=float('inf')):
        MiniMax.__init__(self, depth)
        self.count = 0


    def _utility(self, board, color, alpha, beta, depth, coord, option):
        getattr(board, option)(coord, color)
        alpha = self._search(board, _toggle_color(color),
                             alpha, beta, depth-1)[0]
        board.undo()
        return alpha, (coord, option)


    def _gen_children(self, board, color, depth):
        for move in board.moves(color):
            alpha, beta = (yield)
            yield self._utility(board, color, alpha, beta, depth, *move)

    def search(self, board, color):
        return self._search(board, color, -oo, oo, self._max_depth)[1]

    def _search(self, board, color, alpha, beta, depth):
        self.count += 1
        if depth < 1 or board.full:
            return board.utility, None
        move = None
        children = self._gen_children(board, color, depth)
        try:
            while True:
                next(children)
                child = children.send((alpha, beta))
                if color == "BLUE":
                    alpha, move = max((alpha, move), child,
                                      key=lambda x: x[0])
                    if beta <= alpha:
                        return alpha, move
                else:
                    beta, move = min((beta, move), child,
                                 key=lambda x: x[0])
                    if beta <= alpha:
                        return beta, move
        except StopIteration:
            pass
        if color == "BLUE":
            return alpha, move
        else:
            return beta, move


class TimeSearch(AlphaBeta):
    def _utility(self, board, color, alpha, beta, depth, coord, option):
        getattr(board, option)(coord, color)
        alpha = self._search(board, _toggle_color(color),
                             alpha, beta,
                             depth/(board.width*board.height-board.filled+1))[0]
        board.undo()
        return alpha, (coord, option)


class ForceBlitz(AlphaBeta):
    def _gen_children(self, board, color, depth):
        try:
            moves = board.moves(color)
            while True:
                move = next(moves)
                if move[1] == "blitz":
                    next(moves)
                alpha, beta = (yield)
                yield self._utility(board, color, alpha, beta, depth, *move)
        except StopIteration:
            pass


class BlitzKrieg(TimeSearch, ForceBlitz):
    pass


if __name__ == '__main__':
    import gameboard
    import cProfile
    g = gameboard.GameBoard('../boards/Wallawalla.txt')
    g.drop(('A', 2), "BLUE")
    g.drop(('B', 1), "GREEN")
    print(g)

    a = AlphaBeta(4)
    cProfile.run('a.search(g, "BLUE")')
    print( "AlphaBeta expanded nodes: ", a.count )
    f = ForceBlitz(4)
    cProfile.run('f.search(g, "BLUE")')
    print( "ForceBlitz expanded nodes: ", f.count )
